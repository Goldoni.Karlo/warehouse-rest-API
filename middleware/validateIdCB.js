function validateIdCB(objValidate, cb) {
    // Return false if product with id is not exist in model with product if id exists
    // objValidate={model, _id}
    objValidate.model.findOne({_id: objValidate.id}  , function (err, obj) {
        if (err) {
            console.log(err, 'Error working with DB, perhaps _id is not valid');
            cb(err);
        } else {
            if (obj == undefined) {
                cb(false, false);
            } else {
                cb(false, obj);
            }
        }
    });
}

module.exports = validateIdCB;