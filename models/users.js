var mongoose = require('mongoose');

var usersSchema = new mongoose.Schema({
  firstname: String,
  lastname: String,
  email: String
  });

// Requires population of author
usersSchema.methods.toJSONFor = function(user){
  return {
    id: this._id,
    body: this.body,
    createdAt: this.createdAt
    };
};

mongoose.model('users', usersSchema);
