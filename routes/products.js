var mongoose = require('mongoose');
var router = require('express').Router();
const products = mongoose.model('products');
const categories = mongoose.model ('categories');
// mainModel is for lihgtway to use standart route for many object
const mainModel = products;
const slaveModel = categories;

const validateIdCB = require ('./../middleware/validateIdCB');
const validateBodyHave = require ('./../middleware/validateBodyHave');
const validateBodyEmpty = require ('./../middleware/validateBodyEmpty');

router.get('/', function (req, res) {
    mainModel.find(function (err, users) {
        if (err) {
            res.status(404).send('Error getting from DB');
        } else {
            res.status(200).send(users);
        }
    })
});

router.get('/:_id', function (req, res) {
    mainModel.findById(req.params._id, function (err, user) {
        if (err) {
            res.status(404).send('Error getting from DB');
        } else {
            if (user == undefined) {
                res.status(404).send('Error search Object with _id=' + req.params._id);
            } else {
                res.status(200).send(user);
            }
        }
    })
});

router.delete('/:_id', function (req, res, next) {
   next();
}, function (req, res, next) {
    // Check _id is real in request
    mainModel.findOne({_id: req.params._id}, function (err, ObjById) {
        if (ObjById == undefined) {
            res.status(404).send('No object  with _id=' + req.params._id + ' for deleting');
        } else {
            next();}
   });
}, function (req, res, next) {
    // Deleting by _id        
    mainModel.findOneAndDelete({ _id: req.params._id}, function (err, cates) {
        if (err){
            res.status(404).send('Error deleting Object with _id=' + req.params._id);
        } else {
            res.status(200).send('Object with _id=' + req.params._id + ' deleted succesfully');
        }
            });
});




router.post('/', function (req, res, next) {
    // Create new Object from body  {name:'value', category:'value'}
    next();
}, function (req, res, next) {
    // Check body for right
    let errstr=validateBodyHave(req,['name', 'category']);
    if (!errstr) {
        next();
    } else {
        res.status(404).send(errstr);
    }
}, function (req, res, next) {
    // Check body for empty
    let errstr=validateBodyEmpty(req,['name', 'category']);
    if (!errstr) {
        next();
    } else {
        res.status(404).send(errstr);
    }
}, function (req, res, next) {
    // Check name is unique
    mainModel.findOne({name: req.body.name}, function (err, cat) {
        if (err) {
            console.log('Error checking unique of name', err);
            res.status(404).send('Error checking unique of name');
        } else {
            if (cat!= undefined) {
                // name allready registered
                res.status(404).send('Name ' + req.body.name + ' is allready used');
            } else {
                // name yet not used
                next();
            }
        }
    });
}, function (req, res, next) {
    // Check category id is exist
    slaveModel.findOne({_id: req.body.category}, function (err, cat) {
        if (err) {
            res.status(404).send('Error checking category');
        } else {
            if (cat!= undefined) {
                // category is exist
                next();
            } else {
                // category not exist
                res.status(404).send('Category is not exist');
            }
        }
    });
}, function (req, res) {
    // Create new Object in MongoDB 
    const newObj = new mainModel(req.body);
    newObj.save((err, cat) => {
        if (err) {
           res.status(404).send('Error creating object');
        } else {
           res.status(200).send(newObj);
        }
    });
});


router.patch('/:_id', function (req, res, next) {
    // Updating some fields of Object from body  {name:'value'}
    mainModel.findById(req.params._id, function (err, user) {
        if (err) {
           res.status(404).send('Error getting from DB');
        } else {
            if (user == undefined) {
                res.status(404).send('Error search Object with _id=' + req.params._id);
            } else {
                next();
            }
        }
    })
    }, function (req, res) {
    // Patch Object in MongoDB 
    mainModel.findOneAndUpdate({_id: req.params._id}, {$set: req.body}, function (err, putchObj) {
        if (err) {
           res.status(404).send('error patching (updating) Object');
        } else {
           res.status(201).send(putchObj);
        }
    });
});

router.put('/:_id', function (req, res, next) {
    // Replace Object from body  {name:'value'}
    mainModel.findById(req.params._id, function (err, user) {
        if (err) {
            res.status(404).send('Error getting from DB');
        } else {
            if (user == undefined) {
                res.status(404).send('Error search Object with _id=' + req.params._id);
            } else {
                next();
            }
        }
    })
}, function (req, res) {
    // Replacing Object in MongoDB 
    mainModel.replaceOne({_id: req.params._id}, req.body, function (err, putchObj) {
        if (err) {
        res.status(404).send('error putting Object ' + err);
    } else {
        res.status(201).send(putchObj);
    }
    });
});

module.exports = router;
