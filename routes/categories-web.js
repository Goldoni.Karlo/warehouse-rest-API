var mongoose = require('mongoose');
var router = require('express').Router();
const categories = mongoose.model('categories');
const products = mongoose.model('products');

router.get('/', function (req, res) {
    categories.find(function (err, categories) {
        res.render('categories', {
            categories
        });
    });
});


router.get('/:id', function (req, res) {
    categories.findById(req.params.id, function (err, cat) {
        if (!err) {
            req.categoryName = cat.name;
            req.categoryId = cat._id;
            products.find({
                category: req.params.id
            }, function (err, prods) {
                if (!err) {
                    res.render('category-products', {
                        categoryName: req.categoryName,
                        categoryId: req.categoryId,
                        products: prods
                    });
                }
            });
        }
    });
});


router.post('/', function (req, res, next) {
    // Create new category from body etc we have body {name:'value'}
    // if you need REST API create {name:'value'}  from req.params....
    console.log('POST/categories body=', req.body);
    console.log('POST/categories body.name=', req.body.name)
    next();
}, function reqcheck(req, res, next) {
    // Check name are in request
    let errstr = '';
    console.log('name=', req.body.name);
    if (req.body.name == '') {
        errstr += 'name is empty ';
    }
    if (!errstr) {
        next();
    } else {
        res.status(404).send(errstr);
    }
}, function nameUniqueCheck(req, res, next) {
    // Check name is unique
    categories.findOne({
        name: req.body.name
    }, function (err, cat) {
        if (err) {
            console.log('err checking unique of categorie name', err);
        } else {
            if (cat == undefined) {
                console.log('Undefined', cat);
                next();
            } else {
                console.log('Уже есть', cat);
                res.status(404).send('This category name is allready used! Can not create new!');
                console.log(cat, 'Name for new category is not unique!');
            }
        }
    });
}, function create(req, res) {
    // Create new categorie in MongoDB
    console.log('Creating', req.body);

    const cat = new categories(req.body);
    cat.save((err, cat) => {
        if (err) {
            console.log('err', err);
        } else {
            console.log(cat, 'New category is created from routs/categories');
            // res.status(201).send(cat);
            res.redirect('http://localhost:3001/categories');
        }
    });
});




router.delete('/:_id', function (req, res, next) {
    console.log('Deletion category with id=', req.params._id);
    next();
}, function idreqreal(req, res, next) {
    // Check id is real in request
    categories.findOne({
        _id: req.params._id
    }, function (err, catid) {
        if (catid == undefined) {
            res.status(404).send('No such category Object in MongoDB for deleting');
        } else {
            categories.findOneAndDelete({
                _id: req.params._id
            }, function (err, cates) {
                if (err) {
                    console.log('err deleting from routes/cars', err);
                } else {
                    console.log(cates, 'Object deleted from routes/cars');
                    //res.status(201).send('Category deleted succesfully');
                    res.redirect('http://localhost:3001/categories');
                }
            });
        }
    });
});


router.patch('/:_id', function (req, res, next) {
    //  Get category_id on input
    console.log('Object for update (patch)=', req.params._id);
    console.log('Parameters for update (patch)=', req.body);
    next();
}, function idreqreal(req, res, next) {
    // Check id is real in request
    categories.findOne({
        _id: req.params._id
    }, function (err, catid) {
        if (catid == undefined) {
            res.status(404).send('No such category Object in MongoDB for patching (updating)');
        } else {
            categories.findOneAndUpdate({
                _id: req.params._id
            }, {
                $set: req.body
            }, function (err, cates) {
                if (err) {
                    console.log('error patching (updating) from routes/categories', err);
                } else {
                    console.log(cates, 'Object updated from routes/categories');
                    //res.status(201).send('Category updated succesfully');
                    res.redirect('http://localhost:3001/categories');
                }
            });
        }
    });
});

router.patch('/', function (req, res, next) {
    //  Get category_id on input
    console.log('Object for updatein query=', req.query);
    console.log('Parameters for update (patch)_id=', req.query._id);
    req._id = req.query._id;
    next();
}, function idreqreal(req, res, next) {
    // Check id is real in request
    categories.findOne({
        _id: req.query._id
    }, function (err, catid) {
        if (catid == undefined) {
            res.status(404).send('No such category Object in MongoDB for patching (updating)');
        } else {
            console.log('Must update', catid);
            req.category = catid;
            next();
        }
    })
}, function getDataForPath(req, res) {
    console.log('Updating for', req.category);
    res.render('categorypatch', {
        category: req.category
    })

});

module.exports = router;


/*

function isProductId(id, cb) {
    // Return false if product with id is not exist in DB or category with product if id exists
    categories.findOne({
        "products._id": id
    }, function (err, catid) {
        if (err) {
            console.log(err, 'error working with DB perhaps _id is not valid');
            cb(err)
        } else {
            if (catid == undefined) {
                // console.log('Product with _id=',id, ' not exist. RETURN FALSE');
                cb(false, false);
            } else {
                cb(false, catid);
            }
        }
    });
}


*/