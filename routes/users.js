var mongoose = require('mongoose');
var router = require('express').Router();
const users = mongoose.model('users');
const categories = mongoose.model ('categories');

// mainModel is for lihgtway to use standart route for many object
const mainModel = users;
const slaveModel= categories;

const validateIdCB = require ('./../middleware/validateIdCB');
const validateBodyHave = require ('./../middleware/validateBodyHave');
const validateBodyEmpty = require ('./../middleware/validateBodyEmpty');

router.get('/', function (req, res) {
    mainModel.find(function (err, users) {
        if (err) {
            res.status(404).send('Error getting from DB');
        } else {
            res.status(200).send(users);
        }
    })
});


router.get('/:_id', function (req, res) {
    mainModel.findById(req.params._id, function (err, user) {
        if (err) {
            res.status(404).send('Error getting from DB');
        } else {
            if (user == undefined) {
                res.status(404).send('Error search Object with _id=' + req.params._id);
            } else {
                res.status(200).send(user);
            }
        }
    })
});

router.get('/:_id/categories', function (req, res, next) {
    mainModel.findById(req.params._id, function (err, user) {
        if (err) {
            res.status(404).send('Error getting from DB');
        } else {
            if (user == undefined) {
                res.status(404).send('Error search Object with _id=' + req.params._id);
            } else {
                next();
            }
        }
    })
} , function (req, res) {
    // get categories for user
    slaveModel.find({user:req.params._id}, function (err, objs) {
        if (err) {
            res.status(404).send('Error getting from DB');
        } else {
            if (objs == undefined) {
                res.status(404).send('Error search categories by user with _id=' + req.params._id);
            } else {
                res.status(200).send(objs);
            }
        }
    })
});



router.delete('/:_id', function (req, res, next) {
   next();
}, function (req, res, next) {
    // Check _id is real in request
    mainModel.findOne({_id: req.params._id}, function (err, ObjById) {
        if (ObjById == undefined) {
            res.status(404).send('No object  with _id=' + req.params._id + ' for deleting');
        } else {
            next();}
   });
}, function (req, res, next) {
    // Deleting by _id        
    mainModel.findOneAndDelete({ _id: req.params._id}, function (err, cates) {
        if (err){
            res.status(404).send('Error deleting Object with _id=' + req.params._id);
        } else {
            res.status(200).send('Object with _id=' + req.params._id + ' deleted succesfully');
        }
    });
});




router.post('/', function (req, res, next) {
    // Create new Object from body  {name:'value'}
    next();
}, function (req, res, next) {
    // Check body for right
    let errstr=validateBodyHave(req,['firstname', 'lastname', 'email']);
    if (!errstr) {
        next();
    } else {
        res.status(404).send(errstr);
    }
}, function (req, res, next) {
    // Check body for empty
    let errstr=validateBodyEmpty(req,['firstname', 'lastname', 'email']);
    if (!errstr) {
        next();
    } else {
        res.status(404).send(errstr);
    }
}, function (req, res, next) {
    // Check name email is unique
    mainModel.findOne({email: req.body.email}, function (err, cat) {
        if (err) {
            console.log('Error checking unique of email', err);
            res.status(404).send('Error checking unique of email');
        } else {
            if (cat!= undefined) {
                // email allready registered
                res.status(404).send('Email' + req.body.email + ' is allready registered');
            } else {
                // email yet not used
                next();
            }
        }
    });
}, function (req, res) {
    // Create new Object in MongoDB 
    const newObj = new mainModel({firstname: req.body.firstname, lastname: req.body.lastname, email: req.body.email });
    newObj.save((err, cat) => {
        if (err) {
           res.status(404).send('Error creating Object in DB');
        } else {
           res.status(200).send(newObj);
        }
    });
});


router.patch('/:_id', function (req, res, next) {
    // Updating some fields of Object from body  {name:'value'}
    mainModel.findById(req.params._id, function (err, user) {
        if (err) {
            res.status(404).send('Error getting from DB');
        } else {
            if (user == undefined) {
                res.status(404).send('Error search Object with _id=' + req.params._id);
            } else {
                next();
            }
        }
    })
    }, function (req, res) {
    // Patch Object in MongoDB 
    mainModel.findOneAndUpdate({_id: req.params._id}, {$set: req.body}, function (err, putchObj) {
        if (err) {
          res.status(404).send('Error patching (updating) Object '+ err);
        } else {
          res.status(201).send(putchObj);
        }
    });
});

router.put('/:_id', function (req, res, next) {
    // Replace Object from body  {name:'value'}
    mainModel.findById(req.params._id, function (err, user) {
        if (err) {
            res.status(404).send('Error getting from DB');
        } else {
            if (user == undefined) {
                res.status(404).send('Error search Object with _id=' + req.params._id);
            } else {
                next();
            }
        }
    })
}, function (req, res) {
    // Replacing Object in MongoDB 
    mainModel.replaceOne({_id: req.params._id}, req.body, function (err, putchObj) {
        if (err) {
        res.status(404).send('Error putting Object' + err);
    } else {
        res.status(201).send(putchObj);
    }
    });
});

module.exports = router;

/*
router.get('/validate/:_id', function (req, res){
   validateIdCB({model:mainModel, id:req.params._id}, (err, result) => {
        if (err)
            { console.log(err);
            res.status(404).send (err);}
        else {
            if (result)
                {res.status(200).send(result);}
            else
                {res.status(404).send ('Object not found');}
        }
    });
});
*/
