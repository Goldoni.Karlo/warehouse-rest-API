function validateBodyHave (req, arr)
{
 // Check body have parameters from arr [namt1, name2,... name n]
let errstr='';    
for (let i=0; i<arr.length; i++)
  { 
    if (arr[i] in req.body) {continue;}
    else
    {errstr+=' Parameter '+arr[i]+ ' is undefined';}        
  }
return errstr;
}
module.exports = validateBodyHave;