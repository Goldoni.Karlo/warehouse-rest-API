var mongoose = require('mongoose');
var router = require('express').Router();
const categories = mongoose.model('categories');
const products = mongoose.model ('products');
const users = mongoose.model ('users');

// mainModel, slaveModel, upperModel is for lihgtway to use standart route for linked objects
const mainModel = categories;
const slaveModel = products;
const upperModel = users;

const validateIdCB = require ('./../middleware/validateIdCB');
const validateBodyHave = require ('./../middleware/validateBodyHave');
const validateBodyEmpty = require ('./../middleware/validateBodyEmpty');

router.get('/', function (req, res) {
    mainModel.find(function (err, users) {
        if (err) {
            res.status(404).send('Error getting from DB');
        } else {
            res.status(200).send(users);
        }
    })
});


router.get('/:_id', function (req, res) {
    mainModel.findById(req.params._id, function (err, user) {
        if (err) {
            res.status(404).send('Error getting from DB');
        } else {
            if (user == undefined) {
                res.status(404).send('Error search Object with _id=' + req.params._id);
            } else {
                res.status(200).send(user);
            }
        }
    })
});


router.get('/:_id/products', function (req, res, next) {
    console.log('Search products in category _id=', req.params._id);
    mainModel.findById(req.params._id, function (err, user) {
        if (err) {
            res.status(404).send('Error getting from DB');
        } else {
            if (user == undefined) {
                res.status(404).send('Error search Object with _id=' + req.params._id);
            } else {
                next();
            }
        }
    })
} , function (req, res) {
    // get products for category
    slaveModel.find({category:req.params._id}, function (err, objs) {
        if (err) {
            res.status(404).send('Error getting from DB');
        } else {
            if (objs == undefined) {
                res.status(404).send('Error search products in category with _id=' + req.params._id);
            } else {
                res.status(200).send(objs);
            }
        }
    })
});



router.delete('/:_id', function (req, res, next) {
   next();
}, function (req, res, next) {
    // Check _id is real in request
    mainModel.findOne({_id: req.params._id}, function (err, ObjById) {
        if (ObjById == undefined) {
            res.status(404).send('No object  with _id=' + req.params._id + ' for deleting');
        } else {
            next();}
   });
}, function (req, res, next) {
    // Deleting by _id        
    mainModel.findOneAndDelete({ _id: req.params._id}, function (err, cates) {
        if (err){
            res.status(404).send('Error deleting Object with _id=' + req.params._id);
        } else {
            res.status(200).send('Object with _id=' + req.params._id + ' deleted succesfully');
        }
    });
});




router.post('/', function (req, res, next) {
    // Create new Object from body  {name:'value'}
    next();
}, function (req, res, next) {
    // Check body for right
    let errstr=validateBodyHave(req,['name', 'user']);
    if (!errstr) {
        next();
    } else {
        res.status(404).send(errstr);
    }
}, function (req, res, next) {
    // Check body for empty
    let errstr=validateBodyEmpty(req,['name', 'user']);
    if (!errstr) {
        next();
    } else {
        res.status(404).send(errstr);
    }
}, function (req, res, next) {
    // Check name is unique
    mainModel.findOne({name: req.body.name}, function (err, cat) {
        if (err) {
            res.status(404).send('Error checking unique of name');
        } else {
            if (cat!= undefined) {
                // name allready registered
                res.status(404).send(req.body.name + ' is allready registered as category');
            } else {
                // name yet not used
                next();
            }
        }
    });
}, function (req, res, next) {
    // Check usern is exist
    upperModel.findOne({_id: req.body.user}, function (err, cat) {
        if (err) {
            res.status(404).send('Error checking user');
        } else {
            if (cat!= undefined) {
                // user is not exists
                res.status(404).send('User by _id='+req.body.user+' is not exists');
            } else {
                // user exists
                next();
            }
        }
    });
}, function (req, res) {
    // Create new Object in MongoDB 
    const newObj = new mainModel({name: req.body.name});
    newObj.save((err, cat) => {
        if (err) {
            res.status(404).send('Error creating new object in DB');
        } else {
            res.status(200).send(newObj);
        }
    });
});


router.patch('/:_id', function (req, res, next) {
    // Updating some fields of Object from body  {name:'value'}
    mainModel.findById(req.params._id, function (err, user) {
        if (err) {
            res.status(404).send('Error getting from DB');
        } else {
            if (user == undefined) {
                res.status(404).send('Error search Object with _id=' + req.params._id);
            } else {
                next();
            }
        }
    })
    }, function (req, res) {
    // Patch Object in MongoDB 
    
    console.log(' Patching with ' ,req.body);
    mainModel.findOneAndUpdate({_id: req.params._id}, {$set: req.body}, function (err, putchObj) {
        if (err) {
            console.log('error patching (updating) Object', err);
        } else {
            console.log('Object updated succefully');
            res.status(201).send(putchObj);
        }
    });
});

router.put('/:_id', function (req, res, next) {
    // Replace Object from body  {name:'value'}
    mainModel.findById(req.params._id, function (err, user) {
        if (err) {
            res.status(404).send('Error getting from DB');
        } else {
            if (user == undefined) {
                res.status(404).send('Error search Object with _id=' + req.params._id);
            } else {
                next();
            }
        }
    })
}, function (req, res) {
    // Replacing Object in MongoDB 
    mainModel.replaceOne({_id: req.params._id}, req.body, function (err, putchObj) {
        if (err) {
        res.status(404).send('Error put Object');
    } else {
        res.status(201).send(putchObj);
    }
    });
});

module.exports = router;
